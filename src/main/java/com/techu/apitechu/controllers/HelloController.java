package com.techu.apitechu.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Hola Mundo desde API TechU";
    }

    @RequestMapping("/hello")
    public String greetings() {
        return "Hola Mundo desde API TechU";
    }

    @GetMapping("/hello")
    public String saludar(@RequestParam (value = "name", defaultValue = "Tech U!") String name){
        return String.format("Hola %s ;", name);
    }
}
