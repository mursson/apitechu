package com.techu.apitechu;

import com.techu.apitechu.controllers.HelloController;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ApitechuApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void testCaseHelloController() {
		HelloController helloController = new HelloController();

		assertEquals("Hola Mundo desde API TechU",helloController.index());
		assertEquals("Hola Mundo desde API TechU",helloController.greetings());
		assertEquals("Hola Antonio ;",helloController.saludar("Antonio"));
	}
}
